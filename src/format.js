var format = require('date-fns/format')

var locales = {
  es: require('date-fns/locale/es'),
}

module.exports = function (date, formatStr) {
  return format(date, formatStr, {
    locale: locales[window.__localeId__] // or global.__localeId__
  })
}